# Ignore everything
*
# except the following:
!.gitignore
!*.py
!*.conf
!*.md
# Gitlab files
!VERSION
!LICENSE
# don't ignore those files in subfolders either
!*/
